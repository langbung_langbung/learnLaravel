    install
        - wampp or xampp
        - composer
        - install laravel project

    routing
        - rounte in web.php
        - pass variable to view
            - compact, array

    - query builder
        - example
            - DB::table("tablename")->get();
            - App\Model name::all();
            - App\Model name::find();

        - using namespace
        - using tinker

    - Eloquence
        - App\Model name::all();
        - App\Model name::find();

    - controller
        - function
    
    - route model binding
        - wildcard 
    