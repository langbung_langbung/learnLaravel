<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
class TaskController extends Controller
{
    //
    public function index(){
        $tasks = Task::all();
        // dd($task);
        // return $task;
        return view('task.index',compact("tasks"));
    }

    // public function show($id){
    //     $task = Task::incomplete();
    //     $task = Task::find($id);
    //     return view('task.show',compact("task"));
    // }

    public function show(Task $task){

        // return $task;
        return view('task.show',compact("task"));
    }
}
